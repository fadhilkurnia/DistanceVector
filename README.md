# Distance Vector Simulation

Program ini merupakan program yang digunakan untuk melakukan simulasi Distance Vector Protocol pada suatu jaringan. Proyek ini dibuat untuk memenuhi Tugas Besar 2 Jaringan Komputer, kontributor dalam tugas ini adalah sebagai berikut:
  
  - 13515083 Muhammad Hilmi Asyrofi
  - 13515122 Alif Ijlal Wafi
  - 13515146 Fadhil Imam Kurnia

## Petunjuk Penggunaan Program
Untuk menjalankan program siapkan data input pada folder `data` kemudian jalankan perintah:
```
make
```
pada direktori utama untuk melakukan instalasi. Untuk melakukan kompilasi program dibutuhkan g++, sehingga pastikan g++ telah terinstal dengan baik. Setelah kode terkompilasi maka akan ada program `main` yang dapat dijalankan dengan file input. Untuk menjalankan program tersebut dengan input pada file `data/input` gunakan perintah berikut:

```
./main < data/input
```


## Simulasi node dan pengiriman informasi

Untuk mempermudah implementasi program, jarak dan hoop node direpresentasikan dengan 2 array yang berbeda, yaitu array `distance` dan `hoop`: 
```c++
int distance[1000][1000];
int hoop[1000][1000];
```
untuk `distance[x][y]`, x menunjukkan node sumber dan y menunjukan node tujuan, sehingga nilai yang ditunjuk merupakan jarak dari node x ke node y.

Simulasi pengiriman informasi yang dimasukkan pada input diimplementasikan pada fungsi `sendInfo` :
```c++
void sendInfo(int x, int y) {
    int dist = distance[y][x]; int hop = hoop[y][x];
    if (dist == -1 || hop == -1)
        return;
    for (int i = 0; i < N; i++) {
        if (distance[x][i] != -1 && (distance[x][i]+dist < distance[y][i] || distance[y][i] == -1)) {
                distance[y][i] = distance[x][i]+dist;
                hoop[y][i] = hop;
        } else if (distance[x][i]+dist == distance[y][i] && hop < hoop[y][i]) {
            hoop[y][i] = hop;
        }
    }
}
```
Pada snippet di atas, terlihat bahwa apabila diberikan skenario node x mengirim informasi pada node y, maka yang akan dilakukan adalah : 
* Tidak melakukan apapun apabila tidak tersambung ataupun jaraknya belum diketahui
* Selain itu, maka untuk setiap node pada routing table x dan y akan diupdate jaraknya apabila belum diketahui (-1) atau informasi jarak node i lebih pendek menurut informasi x, sehingga mengikuti jarak dan hoop dari routing table x. 
* Jika ada node memiliki jarak yang sama namun index hoop-nya lebih kecil, maka hoop yang lebih kecil tersebut yang akan diupdate pada routing table.



## Pembagian Tugas
  - 13515083 Muhammad Hilmi Asyrofi : Implementasi Program, Penyesuaian program dengan I/O gradder
  - 13515122 Alif Ijlal Wafi        : Petunjuk penggunaan program, Penjelasan simulasi node dan pengiriman informasi, Jawaban pertanyaan 1 & 2
  - 13515146 Fadhil Imam Kurnia     : Implementasi program, Penyesuaian Program, Petunjuk penggunaan program, Jawaban pertanyaan 1 & 2

## Jawaban Pertanyaan
1.  Apakah perbedaan dari routing protocol distance-vector dan link state? Manakah yang lebih baik untuk digunakan?
    
    Perbedaan paling mendasar pada keduanya adalah pada cara bertukar informasi antar router. Pada routing distance-vector,tiap router saling mengirim informasi pada tetangga-tetangganya mengenai siapa saja yang tersambung dengannya saat itu, serta jarak menuju dirinya. Sedangkan pada routing link state, setiap router saling mengirim informasi kepada 
    seluruh router di jaringan tersebut mengenai siapa saja yang tersambung dengannya dari awal. Dengan demikian, informasi dinamis yang didapat dari keduanya berbeda; distance-vector hanya mengetahui node tujuan yang dapat dijangkaunya namun tidak mengetahui letaknya, sedangkan pada link state setiap router mengetahui posisi router lainnya. Selain itu, topologi yang diketahui router pada distance-vector tidak dijamin full karena hanya mengandung graf terpendek saja (sebagai acuan hopping), sedangkan pada link state topologi yang diketahui 
    penuh karena mengandalkan pengukuran sehingga tidak perlu mereduksi graf topologi.
   
    Distance vector masih dipakai karena alasan historis dengan support protocol yang luas dan kebutuhan resource komputasi yang cukup minim dibandingkan link state, namun link state lebih baik digunakan karena routing juga 
    memperhitungkan kondisi link masing-masing, tidak hanya jumlah hopping. Selain itu, tidak bergantung pada pengubahan graf dalam menentukan routing (dan graf) terpendek juga dapat menghindarkan proses routing dari adanya 
    kemungkinan skenario looping ketika konstruksi graf.

2.  Pada implementasinya saat ini manakah yang lebih banyak digunakan, distance-vector atau link state? Kenapa?

    Secara garis besar routing protocol yang sering digunakan pada WAN dan backbone internet adalah Border Gateway Protocol (BGP) yang masuk dalam kategori distance-vector protocol. Walaupun sebenarnya BGP tidak menerapkan distance-vector secara murni. BGP merupakan exterior routing yang menghubungkan banyak jaringan di berbagai tempat.

    Protokol tersebut banyak digunakan karena alasan stabilitas, dan skalabilitasnya. Karena perubahan jaringan cepat terjadi di internet maka routing protocol yang digunakan harus mampu beradaptasi dengan cepat. Selain itu protocol tersebut juga memiliki banyak hal yang bisa diatur sehingga lebih customable.